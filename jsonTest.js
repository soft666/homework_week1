let keyEyeColor = () => {
    return ['brown', 'green', 'blue']
}

let keyGender = () => {
    return ['male', 'female']
}

exports.keyEyeColor = keyEyeColor
exports.keyGender = keyGender
