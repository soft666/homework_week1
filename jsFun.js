let fs = require("fs");

let readFileJS = (FileName) => {
  return new Promise((resolve, reject) => {
    fs.readFile(FileName, "utf8", (err, data) => {
      if (err) 
        reject(err);
      else 
        resolve(JSON.parse(data));
    });
  });
};

exports.readFileJS = readFileJS

// console.log(readFileJS("homework5-1_eyes.json"));
